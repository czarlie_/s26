const http = require('http')

// create a variable 'port' to store the port number
const port = 6969

// create a variable 'server' that stores the output of the 'createServer' method
const server = http.createServer((request, response) => {
  // accessing the 'greeting' route that returns the message of 'Hello Again'
  // http://localhost:6969/greeting
  // 'request' is an object that is sent via the client(browser)
  // the 'url' property refers to the url or the link of the browser
  if (request.url === '/greeting') {
    response.writeHead(200, { 'Content-type': 'text/plain' })
    response.end('Hello Again')
  } else if (request.url === '/homepage') {
    response.writeHead(200, { 'Content-type': 'text/plain' })
    response.end('This is the homepage.')
  } else {
    response.writeHead(404, { 'Content-type': 'text/plain' })
    response.end('Page not available :(')
  }
  // Mini activity:
  // Access the 'homepage' route and return a message of 'this is the homepage' with a status code 200 and a plain text
  // All other routes will return a message of 'page not available'
  // Give a status code for not found and a plain text.
})

server.listen(port)

// when server is running, console will print the message
console.log(`Server now accessible at localhost: ${port}`)
